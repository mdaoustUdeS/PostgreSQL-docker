REGISTRY_IMAGE_NAME ?= eg_postgresql

build:
	docker build -t $(REGISTRY_IMAGE_NAME) .

run:
	docker run --rm -P --name pg_test eg_postgresql
